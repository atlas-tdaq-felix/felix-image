The latest releases and the nightly of the FELIX software system are included in these images.

To use them:

```
docker run -it gitlab-registry.cern.ch/atlas-tdaq-felix/felix-image/felix:master-rm5-el9
```

you will find setup files under:

```
/felix-master-rm5
```

and lcg under:

```
/otp/lcg
```


To run X-windows on a mac docker:

in XQuartz, preferences, security: “allow connections from network clients".

in one window
```
brew install socat
socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"
```

then start docker with something like:
```
docker run -e DISPLAY=192.168.1.184:0 -it gitlab-registry.cern.ch/atlas-tdaq-felix/felix-image/felix:master-rm5-el9
```



Mark Donszelmann (duns@cern.ch)
